(in-package :icvm)

(declaim (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0))
         (type (simple-array (signed-byte 64)) *mem*)
         (type (unsigned-byte 32) *eip* *erb*))

#+sbcl
(progn
  (sb-ext:defglobal *context* nil)
  (sb-ext:defglobal *mem* (make-array 0 :element-type '(signed-byte 64)))
  (sb-ext:defglobal *eip* 0)
  (sb-ext:defglobal *erb* 0))
#-sbcl
(progn
  (defvar *context*)
  (defvar *mem*)
  (defvar *eip*)
  (defvar *erb*))

(defstruct (icm (:conc-name nil))
  (mem (error "ICM need memory") :type (simple-array (signed-byte 64)))
  (eip 0 :type (unsigned-byte 32))
  (erb 0 :type (unsigned-byte 32))
  (exit-code :initialized :type symbol)
  (infn #'read :type function)
  (outfn #'print :type function))

(defgeneric parse-mem (program)
  (:method ((p pathname))
    (parse-mem (read-file-into-string p)))
  (:method ((s string) &aux (m (mapcar #'parse-integer (split "," s))))
    (make-array (length m) :element-type '(signed-byte 64) :initial-contents m))
  (:method ((a #.(class-of (make-array 0 :element-type '(signed-byte 64)))))
    (copy-seq a)))

(defun expand-mem (size)
  (let ((newmem (make-array size :initial-element 0
                                 :element-type '(signed-byte 64))))
    (setf *mem* (dotimes (i (length *mem*) newmem)
                  (setf (aref newmem i) (aref *mem* i))))))

(defun/type/inln mref ((ref (unsigned-byte 32))) (signed-byte 64)
  (when (>= ref (length *mem*)) 0)
  (aref *mem* ref))

(defun/type/inln (setf mref) ((newval (signed-byte 64)) (ref (unsigned-byte 32))) t
  (when (>= ref (length *mem*))
    (expand-mem (ash ref 1)))
  (setf (aref *mem* ref) newval))

(defun halt (exit-code)
  (throw 'halt exit-code))

(defop 1  (a b r) (setf r (+ a b)))
(defop 2  (a b r) (setf r (* a b)))
(defop 3  (r)     (setf r (input)))
(defop 4  (a)     (output a))
(defop 5  (a b)   (when (not (= 0 a)) (setf *eip* b)))
(defop 6  (a b)   (when (= 0 a) (setf *eip* b)))
(defop 7  (a b r) (setf r (if (< a b) 1 0)))
(defop 8  (a b r) (setf r (if (= a b) 1 0)))
(defop 9  (a)     (incf *erb* a))
(defop 99 ()      (halt :halted))

(defun run-program ()
  (setf (exit-code *context*)
        (catch 'halt
          (unwind-protect (funcall (svref *ops* (mref *eip*)))
            (setf (eip *context*) *eip*
                  (erb *context*) *erb*
                  (mem *context*) *mem*)))))

(defgeneric cpu (icm)
  (:method :around ((icm icm))
    (setf *context* icm
          *erb* (erb icm)
          *eip* (eip icm)
          *mem* (mem icm))
    (call-next-method))
  (:method ((icm icm))
    (run-program)
    icm))

(defmethod print-object ((icm icm) s)
  (print-unreadable-object (icm s :type t :identity t)
    (format s "~A" (exit-code icm))))
