(in-package :icvm)

(declaim (type (simple-vector 22300) *ops*))

#+sbcl
(sb-ext:defglobal *ops*
    (make-array 22300
                :initial-element (lambda ()
                                   (error "invalid opcode ~A" (mref *eip*)))))
#-sbcl
(defparameter *ops*
  (make-array 22300
              :initial-element (lambda ()
                                 (error "invalid opcode ~A" (mref *eip*)))))

(defun valid-modes-p (code)
  (and (>= 2 (mod (floor code 100) 10))
       (>= 2 (mod (floor code 1000) 10))
       (>= 2 (mod (floor code 10000) 10))))

(defmacro defop (op args &body body)
  (let ((ps (1+ (length args)))
        (gsym (gensym)))
    (flet ((deref (offset code)
             `(mref ,(case (mod (floor code (expt 10 (1+ offset))) 10)
                       (1                `(- ,gsym ,(- ps offset)))
                       (0          `(mref (- ,gsym ,(- ps offset))))
                       (2 `(+ *erb* (mref (- ,gsym ,(- ps offset)))))))))
      `(setf
        ,@(loop for code from op to (mod 22299 (expt 10 (1+ ps))) by 100
                when (valid-modes-p code)
                  nconc
                `((svref *ops* ,code)
                  (lambda ()
                    (let ((,gsym (incf *eip* ,ps)))
                      (declare (type (unsigned-byte 32) ,gsym)
                               (ignorable ,gsym))
                      (macrolet ((output (n) `(funcall (outfn *context*) ,n))
                                 (input  ()  `(prog2 (decf *eip* ,',ps)
                                                  (funcall (infn *context*))
                                                (incf *eip* ,',ps))))
                        (symbol-macrolet
                            (,@(loop for pos from 1 below ps
                                     for arg in args
                                     collect `(,arg ,(deref pos code))))
                          ,@body
                          (funcall (svref *ops* (mref *eip*)))))))))))))

(defmacro defun/type (name typed-lambda-list return-type &body body)
  (multiple-value-bind (required-params-typed optional-params)
      (split-lambda-list typed-lambda-list)
    (multiple-value-bind (body declarations doc-string)
        (parse-body body :documentation t)
      (let ((required-params (mapcar (lambda (a) (if (consp a) (car a) a))
                                     required-params-typed))
            (required-param-types (mapcar (lambda (a) (if (consp a) (cadr a) t))
                                          required-params-typed))
            (optional-param-types (mapcar (lambda (a)
                                            (if (member a lambda-list-keywords) a t))
                                          optional-params)))
        `(progn
           (declaim (ftype (function ,(append required-param-types optional-param-types)
                                     ,return-type)
                           ,name))
           (defun ,name ,(append required-params optional-params)
             ,@(append (when doc-string (list doc-string))
                       declarations
                       (loop for r in required-params
                             for type in required-param-types
                             unless (eq type t)
                               collect `(declare (type ,type ,r))))
             (the ,return-type (progn ,@body))))))))

(defun split-lambda-list (lambda-list)
  (loop for cons on lambda-list
        for tail = (cdr cons)
        until (member (car tail) lambda-list-keywords)
        finally (when cons (rplacd cons nil))
                (return (values lambda-list tail))))

(defmacro defun/inln (name lambda-list &body body)
  `(progn
     (declaim (inline ,name))
     (defun ,name ,lambda-list ,@body)))

(defmacro defun/type/inln (name typed-lambda-list return-type &body body)
  `(progn
     (declaim (inline ,name))
     (defun/type ,name ,typed-lambda-list ,return-type ,@body)))
